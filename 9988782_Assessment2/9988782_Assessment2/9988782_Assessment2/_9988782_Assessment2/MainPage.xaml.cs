﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace _9988782_Assessment2
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        

        public MainPage(string a, string b, string c, string d)
        {
            InitializeComponent();

            MovieMethodImage.Source = a;
            Rating.text = b;
            YearofRelease.text = c;
            Overview.Text = d;
        }
    }
}
