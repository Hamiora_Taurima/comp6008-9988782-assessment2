﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace _9988782_Assessment2
{

    public partial class MovieMethod : ContentPage
    {

        public MovieMethod()
        {
            InitializeComponent();


            Pic1.Source = MovieSource.ImgView()[0];
            Pic2.Source = MovieSource.ImgView()[1];
            Pic3.Source = MovieSource.ImgView()[2];
            Pic4.Source = MovieSource.ImgView()[3];
            Pic5.Source = MovieSource.ImgView()[4];
            Pic6.Source = MovieSource.ImgView()[5];
            Pic7.Source = MovieSource.ImgView()[6];
            Pic8.Source = MovieSource.ImgView()[7];
            Pic9.Source = MovieSource.ImgView()[8];
            Pic10.Source = MovieSource.ImgView()[9];
            Pic11.Source = MovieSource.ImgView()[10];
            Pic12.Source = MovieSource.ImgView()[11];
            Pic13.Source = MovieSource.ImgView()[12];
            Pic14.Source = MovieSource.ImgView()[13];
            Pic15.Source = MovieSource.ImgView()[14];
            Pic16.Source = MovieSource.ImgView()[15];
            Pic17.Source = MovieSource.ImgView()[16];
            Pic18.Source = MovieSource.ImgView()[17];
            Pic19.Source = MovieSource.ImgView()[18];
            Pic20.Source = MovieSource.ImgView()[19];

            var push = new TapGestureRecognizer();
            push.Tapped += Tapthat;

            Pic1.GestureRecognizers.Add(push);
            Pic2.GestureRecognizers.Add(push);
            Pic3.GestureRecognizers.Add(push);
            Pic4.GestureRecognizers.Add(push);
            Pic5.GestureRecognizers.Add(push);
            Pic6.GestureRecognizers.Add(push);
            Pic7.GestureRecognizers.Add(push);
            Pic8.GestureRecognizers.Add(push);
            Pic9.GestureRecognizers.Add(push);
            Pic10.GestureRecognizers.Add(push);
            Pic11.GestureRecognizers.Add(push);
            Pic12.GestureRecognizers.Add(push);
            Pic13.GestureRecognizers.Add(push);
            Pic14.GestureRecognizers.Add(push);
            Pic15.GestureRecognizers.Add(push);
            Pic16.GestureRecognizers.Add(push);
            Pic17.GestureRecognizers.Add(push);
            Pic18.GestureRecognizers.Add(push);
            Pic19.GestureRecognizers.Add(push);
            Pic20.GestureRecognizers.Add(push);

            void Tapthat(object sender, EventArgs e)

            {
                int gridrow = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * 4;
                int gridcolumn = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);
                int i = gridrow + gridcolumn;

                Navigation.PushAsync(new MainPage(MovieSource.ImgView()[i].Img, MovieSource.ImgView()[i].YearofRelease.ToString(), MovieSource.ImgView()[i].Rating, MovieSource.ImgView()[i].Overview));
            }

        }

    }
}
