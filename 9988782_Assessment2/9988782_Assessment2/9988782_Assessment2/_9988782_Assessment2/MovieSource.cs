﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Xaml;

namespace _9988782_Assessment2
{
    class MovieSource
    {
        public static List<Info_for_movies> ImgView()
        {

            List<Info_for_movies> details = new List<Info_for_movies>();

            {
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0083922/mediaviewer/rm2575342848", YearofRelease = " 17 December 1982", Rating = "8.1 / 10", Overview = "Two young Swedish children experience the many comedies and tragedies of their family, the Ekdahls." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0325980/mediaviewer/rm1229560064", YearofRelease = " 9 July 2003 ", Rating = "8.0 / 10", Overview = "Blacksmith Will Turner teams up with eccentric pirate Captain Jack Sparrow to save his love, the governor's daughter, from Jack's former pirate allies, who are now undead." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0118694/mediaviewer/rm3776003840", YearofRelease = " 9 March 2001", Rating = "8.1 / 10", Overview = "Two neighbors, a woman and a man, form a strong bond after both suspect extramarital activities of their spouses. However, they agree to keep their bond platonic so as not to commit similar wrongs." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0101414/mediaviewer/rm3497069824", YearofRelease = " 22 November 1991", Rating = "8.0 / 10", Overview = "A young woman whose father has been imprisoned by a terrifying beast offers herself in his place, unaware that her captor is actually a prince, physically altered by a magic spell." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt2948356/mediaviewer/rm3116491008", YearofRelease = " 4 March 2016", Rating = "8.1 / 10", Overview = "n a city of anthropomorphic animals, a rookie bunny cop and a cynical con artist fox must work together to uncover a conspiracy." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0056687/mediaviewer/rm154605568", YearofRelease = " 31 October 1962", Rating = "8.1 / 10", Overview = "A former child star torments her paraplegic sister in their decaying Hollywood mansion." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt1954470/mediaviewer/rm3981377280", YearofRelease = " 2 August 2012", Rating = "8.3 / 10", Overview = "A clash between Sultan and Shahid Khan leads to the expulsion of Khan from Wasseypur, and ignites a deadly blood feud spanning three generations." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt1454029/mediaviewer/rm925546496", YearofRelease = " 10 August 2011", Rating = "8.1 / 10", Overview = "An aspiring author during the civil rights movement of the 1960s decides to write a book detailing the African American maids' point of view on the white families for which they work, and the hardships they go through on a daily basis." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0072890/mediaviewer/rm990909440", YearofRelease = " 25 December 1975", Rating = "8.0 / 10", Overview = "A man robs a bank to pay for his lover's operation; it turns into a hostage situation and a media circus." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0058946/mediaviewer/rm3838512128", YearofRelease = " 20 September 1967", Rating = "8.1 / 10", Overview = "In the 1950s, fear and violence escalate as the people of Algiers fight for independence from the French government." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0036868/mediaviewer/rm3221009152", YearofRelease = " 17 June 1947", Rating = "8.1 / 10", Overview = "Three World War II veterans return home to small-town America to discover that they and their families have been irreparably changed." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0338564/mediaviewer/rm1377308672", YearofRelease = " 12 December 2002", Rating = "8.1 / 10", Overview = "A story between a mole in the police department and an undercover cop. Their objectives are the same: to find out who is the mole, and who is the cop." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0073195/mediaviewer/rm1449540864", YearofRelease = " 20 June 1975", Rating = "8.0 / 10", Overview = "A giant great white shark arrives on the shores of a New England beach resort and wreaks havoc with bloody attacks on swimmers, until a local sheriff teams up with a marine biologist and an old seafarer to hunt the monster down." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0114746/mediaviewer/rm267521536", YearofRelease = " 5 January 1996", Rating = "8.0 / 10", Overview = "In a future world devastated by disease, a convict is sent back in time to gather information about the man-made virus that wiped out most of the human population on the planet." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0113247/mediaviewer/rm3419144960", YearofRelease = " 23 February 1996", Rating = "8.1 / 10", Overview = "24 hours in the lives of three young men in the French suburbs the day after a violent riot." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0072684/mediaviewer/rm1970725120", YearofRelease = " 18 December 1975", Rating = "8.1 / 10", Overview = "An Irish rogue wins the heart of a rich widow and assumes her dead husband's aristocratic position in 18th-century England." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0107048/mediaviewer/rm4118549504", YearofRelease = " 12 February 1993", Rating = "8.0 / 10", Overview = "A weatherman finds himself inexplicably living the same day over and over again." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0032138/mediaviewer/rm103009280", YearofRelease = "| 25 August 1939", Rating = "8.1 / 10", Overview = "Dorothy Gale is swept away from a farm in Kansas to a magical land of Oz in a tornado and embarks on a quest with her new friends to see the Wizard who can help her return home in Kansas and help her friends as well." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0056801/mediaviewer/rm3041196800", YearofRelease = "25 June 1963", Rating = "8.1 / 10", Overview = "A harried movie director retreats into his memories and fantasies." };
                new Info_for_movies { Img = "http://www.imdb.com/title/tt0088247/mediaviewer/rm1817347072", YearofRelease = " 26 October 1984", Rating = "8.0 / 10", Overview = " seemingly indestructible humanoid cyborg is sent from 2029 to 1984 to assassinate a waitress, whose unborn son will lead humanity in a war against the machines, while a soldier from that war is sent to protect her at all costs." };



            }

            return details;
        }
    }
}